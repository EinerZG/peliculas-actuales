package co.edu.uniquindio.android.electiva.peliculasactuales.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

import co.edu.uniquindio.android.electiva.peliculasactuales.R;
import co.edu.uniquindio.android.electiva.peliculasactuales.fragments.AgregarPeliculaFragment;
import co.edu.uniquindio.android.electiva.peliculasactuales.fragments.ListaDePeliculasFragment;
import co.edu.uniquindio.android.electiva.peliculasactuales.util.Utilidades;
import co.edu.uniquindio.android.electiva.peliculasactuales.vo.Pelicula;

public class PeliculasActualesActivity extends AppCompatActivity implements ListaDePeliculasFragment.OnPeliculaSeleccionadaListener, AgregarPeliculaFragment.OnClickAgregarListerner  {

    private ArrayList<Pelicula> peliculas;
    private ListaDePeliculasFragment listaDePeliculasFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utilidades.obtenerLenguaje(this);
        setContentView(R.layout.activity_peliculas_actuales);

        peliculas = new ArrayList();
        peliculas.add(new Pelicula("Interestelar", "2015", "https://www.youtube.com/watch?v=bvC_0foemLY"));
        peliculas.add(new Pelicula("El Padrino", "2015", "https://www.youtube.com/watch?v=yyDUC1LUXSU"));
        peliculas.add(new Pelicula("Regreso al futuro", "2015", "https://www.youtube.com/watch?v=HL1UzIK-flA"));
        peliculas.add(new Pelicula("Titanic", "2014", "https://www.youtube.com/watch?v=bvC_0foemLY"));
        peliculas.add(new Pelicula("Star Wars", "2014", "https://www.youtube.com/watch?v=yyDUC1LUXSU"));
        peliculas.add(new Pelicula("El bueno, el malo y el feo", "2014", "https://www.youtube.com/watch?v=HL1UzIK-flA"));
        peliculas.add(new Pelicula("La Pantera Rosa", "2015", "https://www.youtube.com/watch?v=bvC_0foemLY"));
        peliculas.add(new Pelicula("Interestelar", "2015", "https://www.youtube.com/watch?v=bvC_0foemLY"));
        peliculas.add(new Pelicula("El Padrino", "2015", "https://www.youtube.com/watch?v=yyDUC1LUXSU"));
        peliculas.add(new Pelicula("Regreso al futuro", "2015", "https://www.youtube.com/watch?v=HL1UzIK-flA"));
        peliculas.add(new Pelicula("Titanic", "2014", "https://www.youtube.com/watch?v=bvC_0foemLY"));
        peliculas.add(new Pelicula("Star Wars", "2014", "https://www.youtube.com/watch?v=yyDUC1LUXSU"));
        peliculas.add(new Pelicula("El bueno, el malo y el feo", "2014", "https://www.youtube.com/watch?v=HL1UzIK-flA"));
        peliculas.add(new Pelicula("La Pantera Rosa", "2015", "https://www.youtube.com/watch?v=bvC_0foemLY"));

        listaDePeliculasFragment = (ListaDePeliculasFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragmento_lista_peliculas);

        //listaDePeliculasFragment.setPeliculas(peliculas);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPeliculaSeleccionada(int position) {
        Intent intent = new Intent(PeliculasActualesActivity.this, DetalleDePeliculaActivity.class);
        intent.putExtra("pelicula", peliculas.get(position));
        startActivity(intent);

    }

    /**
     * Permite llamar el método de para agregar peliculas y actualizar el adaptador
     *
     * @param pelicula pelicula que se desea agregar a la lista
     */
    @Override
    public void onAgregar(Pelicula pelicula) {
        listaDePeliculasFragment.agregarPelicula(pelicula);
    }
}
