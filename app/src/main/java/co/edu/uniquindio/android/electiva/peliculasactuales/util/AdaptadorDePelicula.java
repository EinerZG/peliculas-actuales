package co.edu.uniquindio.android.electiva.peliculasactuales.util;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.edu.uniquindio.android.electiva.peliculasactuales.R;
import co.edu.uniquindio.android.electiva.peliculasactuales.fragments.ListaDePeliculasFragment;
import co.edu.uniquindio.android.electiva.peliculasactuales.vo.Pelicula;

/**
 * Created by Einer on 6/04/2016.
 */
public class AdaptadorDePelicula extends RecyclerView.Adapter<AdaptadorDePelicula.PeliculaViewHolder> {

    private ArrayList<Pelicula> peliculas;
    private static OnClickAdaptadorDePelicula listener;

    public AdaptadorDePelicula(ArrayList<Pelicula> peliculas) {
        this.peliculas = peliculas;
    }

    public AdaptadorDePelicula(ArrayList<Pelicula> peliculas, ListaDePeliculasFragment listaDePeliculasFragment) {
        this.peliculas = peliculas;
        listener = (OnClickAdaptadorDePelicula) listaDePeliculasFragment;
    }

    @Override
    public PeliculaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.resumen_de_pelicula, parent, false);
        PeliculaViewHolder peliculaVH = new PeliculaViewHolder(itemView);
        return peliculaVH;
    }

    @Override
    public void onBindViewHolder(PeliculaViewHolder holder, int position) {
        Pelicula pelicula = peliculas.get(position);
        holder.binPelicual(pelicula);
    }

    @Override
    public int getItemCount() {
        return peliculas.size();
    }

    public interface OnClickAdaptadorDePelicula {
        public void onClickPosition(int pos);
    }

    public static class PeliculaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.titulo)
        protected TextView txtTitulo;
        @BindView(R.id.anio)
        protected TextView txtAnio;

        public PeliculaViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            ButterKnife.bind(this, itemView);
        }

        public void binPelicual(Pelicula p) {
            txtTitulo.setText(p.getTitulo());
            txtAnio.setText(p.getAnio());
        }

        @Override
        public void onClick(View v) {
            listener.onClickPosition(getAdapterPosition());
            //Log.d("TAG", "Element " + getAdapterPosition() + " clicked. " + txtTitulo.getText());
        }
    }

}






