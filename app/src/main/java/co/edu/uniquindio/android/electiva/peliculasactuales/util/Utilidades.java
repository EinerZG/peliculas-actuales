package co.edu.uniquindio.android.electiva.peliculasactuales.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.Locale;

import co.edu.uniquindio.android.electiva.peliculasactuales.R;
import co.edu.uniquindio.android.electiva.peliculasactuales.fragments.AgregarPeliculaFragment;
import co.edu.uniquindio.android.electiva.peliculasactuales.vo.Film;
import co.edu.uniquindio.android.electiva.peliculasactuales.vo.Pelicula;

/**
 * Created by Einer on 20/04/2016.
 */
public class Utilidades {

    public final static String MIS_PREFERENCIAS = "MisPreferencias";
    public final static String LENGUAJE_DE_PREFERENCIA = "languaje_preferences";
    public final static String LENGUAJE_ES = "es";
    public final static String LENGUAJE_EN = "en";
    public final static String URL_SERVICIO = "http://10.0.2.2:3000/api/peliculas";
    public final static String URL_SERVICIO_DOS = "http://films-einergauch0.rhcloud.com/film";
    public static final int LISTAR_PELICULAS = 1;
    public static final int AGREGAR_PELICULA = 2;
    public static final int MODIFICAR_PELICULA = 3;
    public static final int ELIMINAR_PELICULA = 4;
    public static final String NO_SE_AGREGO_LA_PELICULA = "NO fue posible agregar la película";

    /**
     * es el encargado de mostrar el dialogo por medio del cual se van a agregar películas
     * *  *
     *
     * @param fragmentManager permite realizar la transacción del dialogo
     * @param nameClass       nombre de la actividad que lo invoco
     */
    public static void mostrarDialigoAgregarPelicula(FragmentManager fragmentManager, String nameClass) {
        AgregarPeliculaFragment dialogaAdFilm = new AgregarPeliculaFragment();
        dialogaAdFilm.setStyle(dialogaAdFilm.STYLE_NORMAL, R.style.MiDialogo);
        dialogaAdFilm.show(fragmentManager, nameClass);
    }

    public static void cambiarIdioma(Context context) {

        SharedPreferences prefs = context.getSharedPreferences(MIS_PREFERENCIAS, context.MODE_PRIVATE);
        String language = prefs.getString(LENGUAJE_DE_PREFERENCIA, LENGUAJE_ES);

        if (language.equals(LENGUAJE_ES)) {
            language = LENGUAJE_EN;
        } else if (language.equals(LENGUAJE_EN)) {
            language = LENGUAJE_ES;
        }

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(LENGUAJE_DE_PREFERENCIA, language);
        editor.commit();

        obtenerLenguaje(context);
    }

    public static void obtenerLenguaje(Context context) {

        SharedPreferences prefs = context.getSharedPreferences(MIS_PREFERENCIAS, context.MODE_PRIVATE);
        String language = prefs.getString(LENGUAJE_DE_PREFERENCIA, LENGUAJE_ES);

        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getApplicationContext().getResources().updateConfiguration(config, null);
    }


    /**
     * Se encarga de converir un String formato JSON a una Película  * @param jsonPelicula string en formato JSON  * @return pelicula resultante de la conversión
     */
    public static Film convertirJSONAPelicula(String jsonPelicula) {
        Gson gson = new Gson();
        Pelicula pelicula = gson.fromJson(jsonPelicula, Pelicula.class);
        Film film = new Film(pelicula.getTitulo(), pelicula.getAnio(), pelicula.getUrlTrailer(), pelicula.getDescripcion());
        return film;
    }

    /**
     * Se encarga de convertir una pelicula en un JSON  * @param pelicula pelicula que se desea transformar
     *
     * @return cadena en formato de json de pélicula
     */
    public static String convertirPeliculaAJSON(Pelicula pelicula) {

        Film film = new Film(pelicula.getTitulo(), pelicula.getAnio(), pelicula.getUrlTrailer(), pelicula.getDescripcion());
        Gson gson = new Gson();
        String json = gson.toJson(film);
        return json;

    }

    /**
     * Muestra un mensaje en pantalla
     *
     * @param mensaje mensaje que se desea mostrar
     * @param context contexto de donde se quiere mostrar el mensaje
     */
    public static void mostrarMensaje(String mensaje, Context context) {
        Toast.makeText(context, mensaje, Toast.LENGTH_LONG).show();
    }

}
