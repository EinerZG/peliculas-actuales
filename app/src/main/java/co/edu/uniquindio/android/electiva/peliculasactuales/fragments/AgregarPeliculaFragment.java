package co.edu.uniquindio.android.electiva.peliculasactuales.fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.edu.uniquindio.android.electiva.peliculasactuales.R;
import co.edu.uniquindio.android.electiva.peliculasactuales.util.Utilidades;
import co.edu.uniquindio.android.electiva.peliculasactuales.vo.Pelicula;

/**
 * A simple {@link Fragment} subclass.
 */
public class AgregarPeliculaFragment extends DialogFragment {

    @BindView(R.id.btn_agregar_pelicula)
    protected Button btnAgregar;
    @BindView(R.id.cmp_add_title)
    protected EditText cmpTitulo;
    @BindView(R.id.cmp_add_year)
    protected EditText cmpAnio;
    @BindView(R.id.cmp_add_url_trailer)
    protected EditText cmpURLTrailer;
    @BindView(R.id.cmp_add_description)
    protected EditText cmpDescripcion;
    private Unbinder unbinder;
    private OnClickAgregarListerner listener;

    /**
     * Se realacina el evento del fragmento con el de la actividad
     * @param context contexto de la actividad asociada
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Activity activity;

        if (context instanceof Activity) {
            activity = (Activity) context;
            try {
                listener = (OnClickAgregarListerner) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString() + " debe implementar la interfaz OnClickAgregarListerner");
            }
        }


    }

    /**
     * Método constructor por defecto
     */
    public AgregarPeliculaFragment() {
        // Required empty public constructor
    }

    /**
     * se iicializa la vista y se agragan loe elmentos a butterknife
     * @param inflater propiedad para inflar el layout
     * @param container contenedor en el que se quiere agregar el elemento
     * @param savedInstanceState instancia que se salvaron
     * @return la vista del fragmento
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getDialog().setTitle("Agregar Dialogo");
        View view = inflater.inflate(R.layout.fragment_agregar_pelicula, container, false);
        unbinder= ButterKnife.bind(this, view);

        return view;
    }


    /**
     * escucha cuando se presiona el boton agregar
     * @param button el boton que fue presionado
     */
    @OnClick(R.id.btn_agregar_pelicula)
    public void onClick(Button button){

        String titulo= cmpTitulo.getText().toString();
        String anio= cmpAnio.getText().toString();
        String descripcion= cmpDescripcion.getText().toString();
        String urlTrailer= cmpURLTrailer.getText().toString();

        if(!titulo.trim().equals("") && !anio.trim().equals("") && !descripcion.trim().equals("") && !urlTrailer.trim().equals("")){
            Pelicula pelicula= new Pelicula(titulo, anio, urlTrailer, descripcion);
            listener.onAgregar(pelicula);
            dismiss();
        }
        else{
            Utilidades.mostrarMensaje(Utilidades.NO_SE_AGREGO_LA_PELICULA, getContext());
        }

    }

    /**
     * See encarga de avisar a la actividad principal cuando se agregan películas
     */
    public interface OnClickAgregarListerner{
        public void onAgregar(Pelicula pelicula);
    }


}
