package co.edu.uniquindio.android.electiva.peliculasactuales.util;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import co.edu.uniquindio.android.electiva.peliculasactuales.vo.Film;
import co.edu.uniquindio.android.electiva.peliculasactuales.vo.Pelicula;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.util.EntityUtils;

/**
 * Created by Einer on 16/05/2016.
 */
public class CRUD {


    public static ArrayList<Pelicula> getListaDePeliculas() {

        ArrayList<Pelicula> peliculas = new ArrayList<>();
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(Utilidades.URL_SERVICIO);
        request.setHeader("content-type", "application/json");

        try {

            HttpResponse resp = httpClient.execute(request);
            String respStr = EntityUtils.toString(resp.getEntity());
            Gson gson = new Gson();
            Type tipoListaFilms = new TypeToken<ArrayList<Pelicula>>() {
            }.getType();
            peliculas = gson.fromJson(respStr, tipoListaFilms);

        } catch (Exception e) {

            Log.v(CRUD.class.getSimpleName(), e.getMessage());
            return null;

        }

        return peliculas;
    }

    public static ArrayList<Pelicula> getListaDePeliculas2() {

        ArrayList<Film> peliculas = new ArrayList<>();
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(Utilidades.URL_SERVICIO_DOS);
        request.setHeader("content-type", "application/json");

        try {

            HttpResponse resp = httpClient.execute(request);
            String respStr = EntityUtils.toString(resp.getEntity());
            Gson gson = new Gson();
            Type tipoListaFilms = new TypeToken<ArrayList<Film>>() {
            }.getType();
            peliculas = gson.fromJson(respStr, tipoListaFilms);

        } catch (Exception e) {

            Log.v(CRUD.class.getSimpleName(), e.getMessage());
            return null;

        }

        return pasarFilmAPeliculas(peliculas);
    }

    public static Film agregarPeliculaAlServicio(String jsonPelicula) {
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(Utilidades.URL_SERVICIO);
        post.setHeader("content-type", "application/json");
        Film pelicula = null;
        try {
            StringEntity entity = new StringEntity(jsonPelicula);
            post.setEntity(entity);
            HttpResponse respose = httpClient.execute(post);
            String resp = EntityUtils.toString(respose.getEntity());
            pelicula = Utilidades.convertirJSONAPelicula(resp);
        } catch (Exception e) {
            Log.e("ServicioRest", "Error! insercion de película " + e.getMessage());
            return null;
        }
        return pelicula;
    }


    public static ArrayList<Pelicula> pasarFilmAPeliculas(ArrayList<Film> films) {

        ArrayList<Pelicula> peliculas = new ArrayList<Pelicula>();

        for (Film p : films) {
            peliculas.add(new Pelicula(p.getId(), p.getTitle(), p.getYear(), p.getUrlTrailer(), p.getDescription()));
        }

        return peliculas;
    }

}
