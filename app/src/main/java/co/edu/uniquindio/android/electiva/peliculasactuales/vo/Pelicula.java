package co.edu.uniquindio.android.electiva.peliculasactuales.vo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Einer on 5/04/2016.
 */
public class Pelicula implements Parcelable{

    private String titulo;
    private String anio;
    private String urlTrailer;
    private String descripcion;
    private String _id;

    public Pelicula(String titulo, String anio) {
        this.titulo = titulo;
        this.anio = anio;
    }

    public Pelicula(String titulo, String anio, String urlTrailer) {
        this.titulo = titulo;
        this.anio = anio;
        this.urlTrailer = urlTrailer;
    }

    public Pelicula(String titulo, String anio, String urlTrailer, String descripcion) {
        this.titulo = titulo;
        this.anio = anio;
        this.urlTrailer = urlTrailer;
        this.descripcion = descripcion;
    }

    public Pelicula(String _id, String titulo, String anio, String urlTrailer, String descripcion) {
        this._id = _id;
        this.titulo = titulo;
        this.anio = anio;
        this.urlTrailer = urlTrailer;
        this.descripcion = descripcion;
    }

    protected Pelicula(Parcel in) {
        titulo = in.readString();
        anio = in.readString();
        urlTrailer = in.readString();
        descripcion = in.readString();
    }

    public static final Creator<Pelicula> CREATOR = new Creator<Pelicula>() {
        @Override
        public Pelicula createFromParcel(Parcel in) {
            return new Pelicula(in);
        }

        @Override
        public Pelicula[] newArray(int size) {
            return new Pelicula[size];
        }
    };

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public String getUrlTrailer() {
        return urlTrailer;
    }

    public void setUrlTrailer(String urlTrailer) {
        this.urlTrailer = urlTrailer;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String get_id() {
        return _id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(titulo);
        dest.writeString(anio);
        dest.writeString(urlTrailer);
        dest.writeString(descripcion);
    }
}
