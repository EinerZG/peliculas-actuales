package co.edu.uniquindio.android.electiva.peliculasactuales.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import co.edu.uniquindio.android.electiva.peliculasactuales.R;
import co.edu.uniquindio.android.electiva.peliculasactuales.fragments.DetalleDePeliculaFragment;
import co.edu.uniquindio.android.electiva.peliculasactuales.vo.Pelicula;

public class DetalleDePeliculaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_de_pelicula);

        DetalleDePeliculaFragment detalleDePelicula = (DetalleDePeliculaFragment) getSupportFragmentManager().findFragmentById(R.id.fragmento_detalle_pelicula);
        Pelicula pelicula = (Pelicula) getIntent().getExtras().get("pelicula");
        detalleDePelicula.mostrarPelicula(pelicula);

    }
}
