package co.edu.uniquindio.android.electiva.peliculasactuales.fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.edu.uniquindio.android.electiva.peliculasactuales.R;
import co.edu.uniquindio.android.electiva.peliculasactuales.activity.PeliculasActualesActivity;
import co.edu.uniquindio.android.electiva.peliculasactuales.util.AdaptadorDePelicula;
import co.edu.uniquindio.android.electiva.peliculasactuales.util.CRUD;
import co.edu.uniquindio.android.electiva.peliculasactuales.util.Utilidades;
import co.edu.uniquindio.android.electiva.peliculasactuales.vo.Pelicula;

/**
 * Se encarga de maneja todo lo referentes a la lista de peliculas
 * @author Einer
 * @version 1.0
 */
public class ListaDePeliculasFragment extends Fragment implements AdaptadorDePelicula.OnClickAdaptadorDePelicula {

    @BindView(R.id.RecView)
    protected RecyclerView listadoDePeliculas;
    private Unbinder unbinder;

    private AdaptadorDePelicula adaptador;
    private ArrayList<Pelicula> peliculas;
    private OnPeliculaSeleccionadaListener listener;

    /**
     * Método constructor de la clase
     */
    public ListaDePeliculasFragment() {
        // Required empty public constructor
    }

    /**
     * Se reaaliza el casting de la actividad con el padre con la interfaz creada
     * @param context contexto de la actividad padre
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Activity activity;

        if (context instanceof Activity) {
            activity = (Activity) context;
            try {
                listener = (OnPeliculaSeleccionadaListener) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString() + " debe implementar la interfaz OnPeliculaSeleccionadaListener");
            }
        }

    }

    /**
     * se indica que el fragmento controlará el menu
     * @param savedInstanceState informacion que se desea salvar
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    /**
     * Se inicaliza la vista del fragmento
     * @param inflater objeto para inflar el layout
     * @param container contenedor en el que se agregar la vista
     * @param savedInstanceState informacion que se desea salvar
     * @return la vista del fragmento
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lista_de_peliculas, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    /**
     * Se inicaliza los controles y se llena el recyclerview
     * @param savedInstanceState informacion que se desea salvar
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        listadoDePeliculas = (RecyclerView) getView().findViewById(R.id.RecView);
        HiloSecundario hiloSecundario = new HiloSecundario(this.getContext());
        hiloSecundario.execute(Utilidades.LISTAR_PELICULAS);

    }


    /**
     * se carga el menu que se desea agregar desde el fragmento
     * @param menu  menu que se desea cargar
     * @param inflater inflador del menu
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    /**
     * se reconoce la opcion seleccinada
     * @param item item del menu que fue presionado
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.menu_agregar) {

            Utilidades.mostrarDialigoAgregarPelicula(getActivity().getSupportFragmentManager(), PeliculasActualesActivity.class.getSimpleName());


        } else if (id == R.id.menu_eliminar) {

            peliculas.remove(0);
            adaptador.notifyItemRemoved(0);

        } else if (id == R.id.menu_modificar) {

            Pelicula aux = peliculas.get(1);
            peliculas.set(1, peliculas.get(2));
            peliculas.set(2, aux);
            adaptador.notifyItemMoved(1, 2);

        } else if (id == R.id.menu_cambiar_idioma) {
            Utilidades.cambiarIdioma(getContext());
            Intent intent = getActivity().getIntent();
            getActivity().finish();
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Se informa a la actividad principal cual fue la película seleccinada
     * @param pos posicion que fue seleccionada del arreglo
     */
    @Override
    public void onClickPosition(int pos) {
        listener.onPeliculaSeleccionada(pos);
    }

    /**
     * Interfa con la que se genera el callback en la actividad principal
     */
    public interface OnPeliculaSeleccionadaListener {
        void onPeliculaSeleccionada(int position);
    }

    /**
     * se encarga de agregar una pelicula y actualizar el adapter
     *
     * @param pelicula pelicula que se desea agregar
     */
    public void agregarPelicula(Pelicula pelicula) {
        peliculas.add(0, pelicula);
        adaptador.notifyItemInserted(0);
    }

    /**
     *
     * @param peliculas
     */
    public void setPeliculas(ArrayList<Pelicula> peliculas) {
        this.peliculas = peliculas;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public class HiloSecundario extends AsyncTask<Integer, Integer, Integer> {

        private ProgressDialog progress;
        private Context context;
        private Pelicula pelicula;

        public HiloSecundario(Context context) {
            this.context = context;
            pelicula = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(context, context.getString(R.string.cargando_peliculas), context.getString(R.string.espere), true);
        }

        @Override
        protected Integer doInBackground(Integer... params) {

            if (params[0] == Utilidades.LISTAR_PELICULAS) {
                setPeliculas(CRUD.getListaDePeliculas2());
            }

            return params[0];
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            progress.dismiss();

            if (integer == Utilidades.LISTAR_PELICULAS) {
                if (peliculas != null){

                    adaptador = new AdaptadorDePelicula(peliculas, ListaDePeliculasFragment.this);
                    listadoDePeliculas.setAdapter(adaptador);
                    listadoDePeliculas.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

                }
            }

        }
    }

}
