package co.edu.uniquindio.android.electiva.peliculasactuales.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import co.edu.uniquindio.android.electiva.peliculasactuales.R;
import co.edu.uniquindio.android.electiva.peliculasactuales.vo.Pelicula;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetalleDePeliculaFragment extends Fragment implements View.OnClickListener {

    private TextView titulo;
    private Pelicula pelicula;
    private Button btnIrATrailer;

    public DetalleDePeliculaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detalle_de_pelicula, container, false);
    }

    public void mostrarPelicula(Pelicula pelicula) {
        this.pelicula = pelicula;
        titulo = (TextView) getView().findViewById(R.id.titulo_de_detalle_pelicula);
        titulo.setText(pelicula.getTitulo());
        btnIrATrailer = (Button) getView().findViewById(R.id.btn_ir_a_trailes);
        btnIrATrailer.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(pelicula.getUrlTrailer()));
        startActivity(intent);
    }


}
